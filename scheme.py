import json
from typing import Tuple

T_SIMPLE = "simple" 
T_GRADIENT = "gradient"
CONFIG = "config.json"

class Scheme:
    def __init__(self, name: str):
        self.name = name
        self.type = ""
        self.fgs = []
        self.bgs = []
        self.seps = []
        self.load_scheme()

    def load_scheme(self) -> None:
        with open(CONFIG, "r") as config_file:
            config = json.load(config_file)
            if self.name in config["schemes"]:
                self.type = config["schemes"][self.name]["type"]
                self.fgs = config["schemes"][self.name]["fg"]
                self.bgs = config["schemes"][self.name]["bg"]
                self.seps = config["schemes"][self.name]["sep"]

    def get_nth_properties(self, i: int, n: int) -> Tuple[str, str, str]:
        if self.type == T_SIMPLE:
            return self.fgs[n % len(self.fgs)], \
                    self.bgs[n % len(self.bgs)], \
                    self.seps[n % len(self.seps)]
        elif self.type == T_GRADIENT:
            return self.gradient_color(self.fgs[0], self.fgs[1], i, n), \
                    self.gradient_color(self.bgs[0], self.bgs[1], i, n), \
                    self.seps[n % len(self.seps)]

    def gradient_color(self, color1: str, color2: str, i: int, n: int) -> str:
        color1_hex, color2_hex = int(color1[1:], 16), int(color2[1:], 16)
        unit = (color1_hex - color2_hex) / (n - 1)
        return "#" + hex(int(color1_hex - unit * i))[2:]
