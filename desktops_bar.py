from najs_bar import NajsBar, Container
from scheme import Scheme
import subprocess
import json
from typing import Tuple, List, Dict


BASH_LOAD_DESKTOPS = ["bspc", "query", "-T", "-m"]
CONTAINER = """%{{F{fg} B{bg}}}{txt}%{{F{bg} B{nxt_bg}}}{sep}"""
DEFAULT_SCHEME = json.loads("""{"fg": "#FFFFFF00", "bg": "#FF1D1F21",
        "sep": ""}""")

CONFIG = "config.json"

Window = str

class Desktop:
    def __init__(self, bspc_info_fun, config, index: int):
        self.bspc_info_fun = bspc_info_fun
        self.config = config
        self.index = index

    def get_title(self) -> str:
        return self.bspc_info_fun()["desktops"][self.index]["name"]

    def update_windows(self) -> str:
        desktop = self.bspc_info_fun()["desktops"][self.index]
        windows = self.get_node_children(desktop["root"])
        win_symbols = []
        for win_name in windows:
            win_symbols.append(self.get_window_symbol(win_name))
        return "".join(win_symbols)

    def get_window_symbol(self, win_name: str) -> str:
        if win_name in self.config["symbols"]:
            return self.config["symbols"][win_name]
        return "X"

    def get_node_children(self, node) -> List[str]:
        if node is None:
            return []
        if node["firstChild"] is None and node["secondChild"] is None:
            if "client" in node:
                return [node["client"]["className"]]
        return self.get_node_children(node["firstChild"]) \
                + self.get_node_children(node["secondChild"])


class NajsBarDesktops:
    def __init__(self, id_: str, scheme_name: str):
        self.config = None
        self.bspc_info = {}
        self.scheme = Scheme(scheme_name)
        self.bar = NajsBar(id_)

    def create_containers(self) -> None:
        for i in range(len(self.bspc_info["desktops"])):
            desktop = Desktop(self.get_bspc_info, self.config, i)
            title = desktop.get_title()
            container = Container(title, desktop.update_windows, self.scheme)
            self.bar.add_container(container)

    def get_bspc_info(self):
        self.load_desktops()
        return self.bspc_info

    def load_desktops(self) -> None:
        result = subprocess.run(BASH_LOAD_DESKTOPS,
                stdout=subprocess.PIPE, text=True)
        self.bspc_info = json.loads(result.stdout)

    def load_config(self) -> None:
        with open(CONFIG, "r") as config_file:
            self.config = json.loads(config_file.read())
        self.scheme.load_scheme()

bar = NajsBarDesktops("1", "powerline_desktops")
bar.load_desktops()
bar.load_config()
bar.create_containers()
bar.bar.run(100)
