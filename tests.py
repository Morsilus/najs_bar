from najs_bar import NajsBar, Container

TEST_SCHEME = ["#FF1D1F21", "#FFFFFF00", "#FFFFFF00", "", "#FF000000"]

class TestBar:
    def __init__(self, id_: str):
        self.id_ = id_
        self.text = ""

    def get_text(self) -> str:
        return self.text

bar = NajsBar("1")
test_bar = TestBar("1")
test_bar.text = "test text"
bar.add_container(Container("title", test_bar.get_text, TEST_SCHEME))
bar.tick()
