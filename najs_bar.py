import json
from time import sleep
from typing import Tuple, List, Dict

FEEDER_DIR = "/tmp/lemonbar/"

class NajsBar:
    def __init__(self, id_: str):
        self.id_ = id_
        self.containers: List["Container"] = []

    def add_container(self, container: "Container") -> None:
        self.containers.append(container)

    def run(self, tick_ms: int) -> None:
        while True:
            self.tick()
            sleep(tick_ms / 1000)

    def tick(self) -> None:
        source = [container.get_source() for container in self.containers]
        with open(FEEDER_DIR + self.id_, "w") as feeder:
            feeder.write("".join(source))

# background, title, text, separator, next background
Scheme = Tuple[str, str, str, str, str]

class Container:
    def __init__(self, title: str, text_fun, scheme: Scheme):
        self.title = title
        self.text_fun = text_fun
        self.scheme = scheme

    def get_source(self) -> str:
        text = self.text_fun()
        return f"%{{B{self.scheme[0]}}}" \
                f"%{{F{self.scheme[1]}}}" \
                f"{self.title}" \
                f"%{{F{self.scheme[2]}}}" \
                f"{text}" \
                f"%{{F{self.scheme[0]} B{self.scheme[4]}}}" \
                f"{self.scheme[3]}"
