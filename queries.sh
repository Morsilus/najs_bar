#! /bin/bash

sep_left=""
sep_right=""
sep_l_right=""
color_back="#FF1D1F21"
color_back_active="#FF1D1F21"
color_text="#FFFFFF00"
color_text_inactive="#FFFFFF00"

container () {
	# $1 = text
	# $2 = separator
	# $3 = text color
	# $4 = background color
	# $5 = next background color
	if [ $# -eq 5 ]; then
		echo "%{F$3 B$4}$1%{F$4 B$5}$2"
	fi
}

#str=$(container "random text" ${sep_right} ${color_back} ${color_text} ${color_back})


get_window_class() {
	return xprop -id $1 | awk '/WM_CLASS/{$1=$2=$3="";print}' | cut -d '"' -f2
}

get_desktop_ids() {
	bspc query -D
}

get_desktop_names() {
	bspc query -D --names
}

$1
